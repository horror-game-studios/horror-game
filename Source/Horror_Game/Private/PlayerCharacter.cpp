// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AutoPossessPlayer = EAutoReceiveInput::Player0;


	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	Camera->SetRelativeLocation(FVector(0, 0, 70));
	Camera->bUsePawnControlRotation = true;

	FlashLight = CreateDefaultSubobject<USpotLightComponent>(TEXT("Flash Light"));
	FlashLight->AttachToComponent(Camera, FAttachmentTransformRules::KeepRelativeTransform);
	FlashLight->SetRelativeLocation(FVector(0, 0, 0));
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &APlayerCharacter::Interact);
	PlayerInputComponent->BindAction("FlashLight", IE_Pressed, this, &APlayerCharacter::ToggleFlashLight);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::HorizontalMovement);
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::ForwardMovement);
	PlayerInputComponent->BindAxis("Turn", this, &APlayerCharacter::Turn);
	PlayerInputComponent->BindAxis("LookUp", this, &APlayerCharacter::Pitch);
}

void APlayerCharacter::Interact()
{
}

void APlayerCharacter::ToggleFlashLight()
{
	FlashLight->ToggleVisibility();
}

void APlayerCharacter::HorizontalMovement(float value)
{
	if (value)
	{
		AddMovementInput(GetActorRightVector(), value);
	}
}

void APlayerCharacter::ForwardMovement(float value)
{
	if (value)
	{
		AddMovementInput(GetActorForwardVector(), value);
	}
}

void APlayerCharacter::Turn(float value)
{
	if (value)
	{
		AddControllerYawInput(MouseSensitivity * value * GetWorld()->GetDeltaSeconds());
	}
}

void APlayerCharacter::Pitch(float value)
{
	if (value)
	{
		AddControllerPitchInput(MouseSensitivity * value * GetWorld()->GetDeltaSeconds());
	}
}
