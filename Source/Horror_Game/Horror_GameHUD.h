// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Horror_GameHUD.generated.h"

UCLASS()
class AHorror_GameHUD : public AHUD
{
	GENERATED_BODY()

public:
	AHorror_GameHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

