// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Horror_Game : ModuleRules
{
	public Horror_Game(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
