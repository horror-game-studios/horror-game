// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Horror_GameGameMode.generated.h"

UCLASS(minimalapi)
class AHorror_GameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHorror_GameGameMode();
};



