// Copyright Epic Games, Inc. All Rights Reserved.

#include "Horror_GameGameMode.h"
#include "Horror_GameHUD.h"
#include "Horror_GameCharacter.h"
#include "PlayerCharacter.h"
#include "UObject/ConstructorHelpers.h"

AHorror_GameGameMode::AHorror_GameGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	DefaultPawnClass = APlayerCharacter::StaticClass();

	// use our custom HUD class
	HUDClass = AHorror_GameHUD::StaticClass();
}
